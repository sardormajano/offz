import React from 'react';

import App from './components/App.jsx';
import MainTemplate from './components/MainTemplate.jsx';
import Help from './components/Help.jsx';
import Success from './components/Success.jsx';
import Fail from './components/Fail.jsx';

import {Router, Route, IndexRoute, browserHistory} from 'react-router';

export const renderRoutes = () => (
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={MainTemplate} />
      <Route path="help" component={Help} />
      <Route path="success" component={Success} />
      <Route path="fail" component={Fail} />
    </Route>
  </Router>
);
