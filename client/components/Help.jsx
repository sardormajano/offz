import React, {Component} from 'react';
import {addScript, qs, qsa} from '../lib/coreLib.js';

export default class Help extends Component {
  constructor(props) {
    super(props);

    this.state = {
      sig: '018f7778a98ae9edc49d60c11ebf6741'
    };
  }

  payboxAmountChangeHandler(e) {
    e.preventDefault();

    const pgAmount = parseInt(qs('#pg-amount-input').value),
          pgCurrency = qs('#pg-currency-input').value,
          pgDescription = qs('#pg-description-input').value,
          pgLanguage = qs('#pg-language-input').value,
          pgMerchantId = qs('#pg-merchant-id-input').value,
          pgSalt = qs('#pg-salt-input').value,
          pgSK = 'vivufyrujocylero',
          pgSigText = `payment.php;${pgAmount};${pgCurrency};${pgDescription};${pgLanguage};${pgMerchantId};${pgSalt};${pgSK}`,
          pgSig = md5(pgSigText),
          pgSigInput = qs('#pg-sig-input');

    this.setState({sig: pgSig});
  }

  webmoneyAmountChangeHandler(e) {
    e.preventDefault();


  }

  freeKassaSubmitButtonHandler(e) {
    e.preventDefault();

    const form = new FK(),
          merchandId = '29569',
          amount = parseInt(qs('#free-kassa-amount').value),
          orderId = '12345',
          sw = '31kprpb9',
          sign = md5(merchandId + sw);

    form.loadWidget({
        merchant_id: merchandId,
        amount: amount,
        order_id: orderId,
        sign: sign
    });
  }

  componentDidMount() {
    const scripts = [
      { src: 'assets/vendor/jquery/jquery.min.js'},
      { src: 'assets/vendor/bootstrap/js/bootstrap.min.js'},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'},
      { src: 'assets/vendor/scrollreveal/scrollreveal.min.js'},
      { src: 'assets/vendor/magnific-popup/jquery.magnific-popup.min.js'},
      { src: 'assets/js/creative.min.js'},
    ]

    scripts.forEach((script) => {
      addScript(script);
    });
  }

  render() {
    return (
        <div>
          <nav id="mainNav" className="navbar navbar-default navbar-fixed-top">
            <div className="container-fluid">
              {/* Brand and toggle get grouped for better mobile display */}
              <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span className="sr-only">Toggle navigation</span> Menu <i className="fa fa-bars" />
                </button>
                <a className="navbar-brand page-scroll" href="#page-top">ОФ ФЗ</a>
              </div>
              {/* Collect the nav links, forms, and other content for toggling */}
              <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul className="nav navbar-nav navbar-right">
                  <li>
                    <a className="page-scroll" href="#about">Выберите способ оплаты</a>
                  </li>
                  <li>
                    <a className="page-scroll" href="#contact">Контакты</a>
                  </li>
                </ul>
              </div>
              {/* /.navbar-collapse */}
            </div>
            {/* /.container-fluid */}
          </nav>
          <header style={{height: '450px'}}>
            <div className="header-content">
              <div style={{background: 'rgba(1, 84, 157, .5)'}} className="header-content-inner">
                <hr/>
                <p>
                  Согласно Законодательству РК за одну транзакцию вы можете отправить сумму эквивалент
                  212 000 тенге. Количество транзакций с одной платежной карты не ограниченно.
                  Благодарим Вас за помощь.
                </p>
                <hr/>
                <p>
                  According to the legislation of Kazakhstan a single transaction, you can send
                  the amount equivalent 212 000 KZT. The number of transactions from one payment card
                  is not limited. Thank You for your help.
                </p>
                <hr/>
              </div>
            </div>
          </header>
          <section className="bg-primary" id="about">
            <div className="container">
              <h2 className="col-xs-12 text-center section-heading">Выберите способ оплаты.</h2><br/><br/>
              <hr/>
              <div className="row">
                <div className="col-xs-3">
                  <form className="form-inline text-center" method="post" action="https://wl.walletone.com/checkout/checkout/Index">
                    <div className="form-group">
                      <label>Оплата через Единую кассу</label>
                      <div className="input-group">
                        <div className="input-group-addon">$</div>
                        <input name="WMI_PAYMENT_AMOUNT" type="number" className="form-control" id="exampleInputAmount" defaultValue="100.00" />
                        <input name="WMI_MERCHANT_ID" hidden defaultValue={173002654756} />
                        <input name="WMI_CURRENCY_ID" hidden defaultValue={840} />
                        <input name="WMI_DESCRIPTION" hidden defaultValue="Пожертвование" />
                        <input name="WMI_SUCCESS_URL" hidden defaultValue="http://offz.kz/success/" />
                        <input name="WMI_FAIL_URL" hidden defaultValue="http://offz.kz/fail/" />
                      </div>
                    </div>
                    <br/><br/>
                    <button type="submit" className="btn btn-warning">Перевести</button>
                  </form>
                </div>
                <div className="col-xs-3">
                  <form className="form-inline text-center">
                    <div className="form-group">
                      <label>Оплата через Free Kassa</label>
                      <div className="input-group">
                        <div className="input-group-addon">$</div>
                        <input type="number" className="form-control" id="free-kassa-amount" defaultValue="100.00" />
                      </div>
                    </div>
                    <br/><br/>
                    <button onClick={this.freeKassaSubmitButtonHandler.bind(this)} type="submit" className="btn btn-warning">Перевести</button>
                  </form>
                </div>
                <div className="col-xs-3">
                  <form className="form-inline text-center" method="GET" action="https://paybox.kz/payment.php">
                    <div className="form-group">
                      <label>Оплата через Paybox</label>
                      <div className="input-group">
                        <div className="input-group-addon">$</div>
                        <input id="pg-amount-input" name="pg_amount" onChange={this.payboxAmountChangeHandler.bind(this)} type="number" className="form-control" defaultValue="100" />
                        <input id="pg-currency-input" name="pg_currency" hidden defaultValue='USD' />
                        <input id="pg-description-input" name="pg_description" hidden defaultValue='Пожертвование' />
                        <input id="pg-language-input" name="pg_language" hidden defaultValue='ru' />
                        <input id="pg-merchant-id-input" name="pg_merchant_id" hidden defaultValue='10249' />
                        <input id="pg-salt-input" name="pg_salt" hidden defaultValue='aaaaaaaaaa' />
                        <input id="pg-sig-input" name="pg_sig" hidden value={this.state.sig} readOnly />
                      </div>
                    </div>
                    <br/><br/>
                    <button type="submit" id="paybox-submit-button" className="btn btn-warning">Перевести</button>
                  </form>
                </div>
                <div className="col-xs-3">
                  <form className="form-inline text-center" method="POST" action="https://merchant.webmoney.ru/lmi/payment.asp">
                    <div className="form-group">
                      <label>Оплата через Webmoney</label>
                      <div className="input-group">
                        <div className="input-group-addon">$</div>
                        <input name="LMI_PAYMENT_AMOUNT" type="number" className="form-control" defaultValue="100" />
                        <input type="hidden" name="LMI_PAYMENT_DESC_BASE64" defaultValue="0LHQu9Cw0LPQvtGC0LLQvtGA0LjRgtC10LvRjNC90L7RgdGC0Yw=" />
                        <input type="hidden" name="LMI_PAYEE_PURSE" defaultValue="K483232746880" />
                      </div>
                    </div>
                    <br/><br/>
                    <button type="submit" id="webmoney-submit-button" className="btn btn-warning">Перевести</button>
                  </form>
                </div>
              </div>
              <br /><br /><hr />
              <div className="row">
                <div className="col-xs-offset-4 col-xs-4 text-center">
                  <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" defaultValue="_s-xclick" />
                    <input type="hidden" name="hosted_button_id" defaultValue="ZRJ8PLJUKGX58" />
                    <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_donateCC_LG.gif" border={0} name="submit" alt="PayPal - The safer, easier way to pay online!" />
                    <img alt border={0} src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width={1} height={1} />
                  </form>
                </div>
              </div>
            </div>
          </section>
          <section id="contact">
            <div className="container">
              <div className="row">
                <div className="col-lg-8 col-lg-offset-2 text-center">
                  <h2 className="section-heading">Свяжитесь с нами!</h2>
                  <hr className="primary" />
                  <p><b>Адрес:</b> Астана ул. Бейбитшилик 14, ВП-2</p>
                </div>
                <div className="col-lg-4 col-lg-offset-2 text-center">
                  <i className="fa fa-phone fa-3x sr-contact" />
                  <p>+7 707 960 65 65</p>
                </div>
                <div className="col-lg-4 text-center">
                  <i className="fa fa-envelope-o fa-3x sr-contact" />
                  <p><a href="mailto:offkz@yandex.ru">offkz@yandex.ru</a></p>
                </div>
              </div>
            </div>
          </section>
        </div>
    );
  }
}
