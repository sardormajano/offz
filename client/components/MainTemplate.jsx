import React, {Component} from 'react';
import {addScript, qs, qsa} from '../lib/coreLib.js';

export default class MainTemplate extends Component {
  componentDidMount() {
    /*
      <script src="vendor/jquery/jquery.min.js"></script>

      <!-- Bootstrap Core JavaScript -->
      <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
      <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

      <!-- Theme JavaScript -->
      <script src="js/creative.min.js"></script>
    */
    const scripts = [
      { src: 'assets/vendor/jquery/jquery.min.js'},
      { src: 'assets/vendor/bootstrap/js/bootstrap.min.js'},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'},
      { src: 'assets/vendor/scrollreveal/scrollreveal.min.js'},
      { src: 'assets/vendor/magnific-popup/jquery.magnific-popup.min.js'},
      { src: 'assets/js/creative.min.js'},
    ]

    scripts.forEach((script) => {
      addScript(script);
    });
  }
  render() {
    return (
      <div>
        <nav id="mainNav" className="navbar navbar-default navbar-fixed-top">
          <div className="container-fluid">
            {/* Brand and toggle get grouped for better mobile display */}
            <div className="navbar-header">
              <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span className="sr-only">Toggle navigation</span> Menu <i className="fa fa-bars" />
              </button>
              <a className="navbar-brand page-scroll" href="#page-top">ОФ ФЗ</a>
            </div>
            {/* Collect the nav links, forms, and other content for toggling */}
            <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <ul className="nav navbar-nav navbar-right">
                <li>
                  <a className="page-scroll" href="#about">О нас</a>
                </li>
                <li>
                  <a className="page-scroll" href="#services">Программа</a>
                </li>
                <li>
                  <a className="page-scroll" href="#portfolio">Законы казахстана</a>
                </li>
                <li>
                  <a className="page-scroll" href="#contact">Контакты</a>
                </li>
              </ul>
            </div>
            {/* /.navbar-collapse */}
          </div>
          {/* /.container-fluid */}
        </nav>
        <header style={{height: '450px'}}>
          <div className="header-content">
            <div style={{background: 'rgba(1, 84, 157, .5)'}} className="header-content-inner">
              <h1 className="hidden-xs hidden-sm" id="homeHeading" style={{fontSize: '20px', paddingTop: '20px'}}>ОФ «Федерация спортивного и традиционного Ушу города Астаны»</h1>
              <h1 className="visible-xs-block visible-sm" id="homeHeading" style={{fontSize: '20px', paddingTop: '20px'}}>ОФ «Федерация спортивного и традиционного УШУ города Астаны»</h1>
              <hr />
              <p className="hidden-xs hidden-sm" style={{fontSize: '16px', color: 'white', fontWeight: 'bold'}}>
                Цель объединения состоит в том, чтобы обеспечить социальную защищенность инвалидов и пенсионеров в Казахстане, создать необходимые условия, которые дадут им возможность реализовать права и свободы человека и гражданина, и вести полноценный образ жизни.
              </p>
              <p className="visible-xs-block visible-sm" style={{fontSize: '16px', color: 'white', fontWeight: 'bold'}}>
                Обеспечим защищенность инвалидов и пенсионеров в Казахстане.
              </p>
              <a href="help" className="btn btn-primary btn-xl page-scroll">Помочь</a>
            </div>
          </div>
        </header>
        <section className="bg-primary" id="about">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 col-lg-offset-2 text-center">
                <h2 className="section-heading">Сделаем это вместе!</h2>
                <hr className="light" />
                <p className="text-faded">
                  Несмотря на определенные усилия органов государственной власти и местного самоуправления, в Казахстанском обществе еще мало изменились стереотипы в отношении к инвалидам и пенсионеров - цель нашего объединения способствовать созданию условий, где инвалиды и
                  пенсионеры получат равных прав и равных возможностей в Казахстанском обществе.
                </p>
                <a href="help" className="page-scroll btn btn-default btn-xl sr-button">Помочь</a>
              </div>
            </div>
          </div>
        </section>
        <section id="services">
          <div className="container">
            <div className="row">
              <div className="col-lg-12 text-center">
                <h2 className="section-heading">В ваших руках</h2>
                <hr className="primary" />
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-6 text-center">
                <div className="service-box">
                  <img width='400' className="sr-icons"src='assets/img/portfolio/servicebox/1.png'/>
                  <h3>СОЦИАЛЬНАЯ ЗАЩИТА ГРАЖДАН</h3>
                  <p className="text-muted" style={{textAlign: 'justify'}}>
                    Обеспечение равных прав и возможностей инвалидов и их социальной защиты, выявление,
                    устранение препятствий и барьеров, препятствующих обеспечению прав и удовлетворению
                    потребностей таких лиц, в том числе в отношении доступа их наравне с другими гражданами
                    к объектам физического окружения
                  </p>
                </div>
              </div>
              <div className="col-md-6 text-center">
                <div className="service-box">
                  <img width='400' className="sr-icons"src='assets/img/portfolio/servicebox/2.png'/>
                  <h3>ОБРАЗОВАНИЕ И ПРОФ. ПОДГОТОВКА</h3>
                  <p className="text-muted" style={{textAlign: 'justify'}}>
                    Реализация творческих и производственных способностей инвалидов и с учетом индивидуальных
                    программ реабилитации, обеспечения прав инвалидов на трудоустройство и оплачиваемую работу,
                    в том числе с условием о выполнении работы на дому.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-lg-12 text-center">
                <h2 className="section-heading">А так же</h2>
                <hr className="primary" />
              </div>
            </div>
          </div>
          <div className="container">
            <div className="row">
              <div className="col-md-6 text-center">
                <div className="service-box">
                  <img width='400' className="sr-icons"src='assets/img/portfolio/servicebox/3.png'/>
                  <h3>ЖИЛИЩНОЕ ОБЕСПЕЧЕНИЕ</h3>
                  <p className="text-muted" style={{textAlign: 'justify'}}>
                    Отстаивания прав на обеспечение жилой площадью лиц, нуждающихся в улучшении жилищных условий,
                    и отвода земельных участков для индивидуального жилищного строительства, садоводства и
                    огородничества, первоочередной ремонт жилых домов и квартир этих лиц и обеспечение их топливом.
                  </p>
                </div>
              </div>
              <div className="col-md-6 text-center">
                <div className="service-box">
                  <img width='400' className="sr-icons"src='assets/img/portfolio/servicebox/4.png'/>
                  <h3>МАТЕРИАЛЬНАЯ ПОМОЩЬ</h3>
                  <p className="text-muted" style={{textAlign: 'justify'}}>
                    Слежение за выполнением материального обеспечения граждане пожилого возраста, имеют право на
                    материальное обеспечение в соответствии с действующим законодательством. Это право гарантируется
                    выплатой пенсий, различных видов помощи и оказанием помощи в натуральном виде.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section id="problem">
          <div className="container">
            <div className="row">
              <img className="hidden-xs hidden-sm col-sm-12 col-lg-6" src="assets/img/portfolio/social-protection/1.png" />
              <hr className="hidden-lg" />
              <div className="col-sm-12 col-lg-6">
                <div className="col-lg-12 text-center">
                  <h2 className="section-heading">Проблема социальной защиты</h2>
                  <hr className="primary" />
                </div>
                <div className="col-lg-12 text-center">
                  <p style={{textAlign: 'justify'}}>
                    В каждой стране есть граждане с инвалидностью и они имеют право владеть всей полнотой социально-экономических,
                    политических, личных прав и свобод.
                  </p>
                  <p style={{textAlign: 'justify'}}>
                    Инвалидом является лицо со стойким расстройством функций организма, при взаимодействии с внешней средой может
                    приводить к ограничению его жизнедеятельности, в результате чего государство обязано создать условия для
                    реализации им прав наравне с другими гражданами и обеспечить ее социальную защиту.
                  </p>
                  <p style={{textAlign: 'justify'}}>
                    Теперь можно говорить о глобальном характере проблемы инвалидности - во всем мире те или иные возможности
                    ограничены примерно у каждого десятого человека (650 млн человек), из них почти 470 млн человек трудоспособного
                     возраста. Для Казахстана проблема социальной защиты инвалидов является особо значимой в связи со стойкой
                     тенденцией к росту доли инвалидов в общей структуре населения.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <section className="no-padding" id="portfolio">
          <div className="container-fluid">
            <div className="row no-gutter popup-gallery">
              <div className="col-lg-3 col-sm-6">
                <a href="assets/img/portfolio/fullsize/1.jpg" className="portfolio-box">
                  <img src="assets/img/portfolio/thumbnails/1.jpg" className="img-responsive" alt />
                  <div className="portfolio-box-caption">
                    <div className="portfolio-box-caption-content">
                      <div className="project-category text-faded">
                        Закон Казахстана:
                      </div>
                      <div className="project-name">
                        Об основах социальной защищенности инвалидов в Казахстане
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div className="col-lg-3 col-sm-6">
                <a href="assets/img/portfolio/fullsize/2.jpg" className="portfolio-box">
                  <img src="assets/img/portfolio/thumbnails/2.jpg" className="img-responsive" alt />
                  <div className="portfolio-box-caption">
                    <div className="portfolio-box-caption-content">
                      <div className="project-category text-faded">
                        Закон Казахстана:
                      </div>
                      <div className="project-name">
                        Об основных принципах социальной защиты ветеранов труда и других граждан преклонного возраста в Казахстане
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div className="col-lg-3 col-sm-6">
                <a href="assets/img/portfolio/fullsize/3.jpg" className="portfolio-box">
                  <img src="assets/img/portfolio/thumbnails/3.jpg" className="img-responsive" alt />
                  <div className="portfolio-box-caption">
                    <div className="portfolio-box-caption-content">
                      <div className="project-category text-faded">
                        Закон Казахстана:
                      </div>
                      <div className="project-name">
                        О государственной социальной помощи инвалидам с детства и детям-инвалидам
                      </div>
                    </div>
                  </div>
                </a>
              </div>
              <div className="col-lg-3 col-sm-6">
                <a href="assets/img/portfolio/fullsize/4.jpg" className="portfolio-box">
                  <img src="assets/img/portfolio/thumbnails/4.jpg" className="img-responsive" alt />
                  <div className="portfolio-box-caption">
                    <div className="portfolio-box-caption-content">
                      <div className="project-category text-faded">
                        Закон Казахстана:
                      </div>
                      <div className="project-name">
                        О реабилитации инвалидов в Казахстане
                      </div>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>
        </section>
        <aside className="bg-dark">
          <div className="container text-center">
            <div className="call-to-action">
              <h2>Поможем этим людям, стать полноценной частью общества!</h2>
              <a href="help" className="btn btn-default btn-xl sr-button">Помочь!</a>
            </div>
          </div>
        </aside>
        <section id="contact">
          <div className="container">
            <div className="row">
              <div className="col-lg-8 col-lg-offset-2 text-center">
                <h2 className="section-heading">Свяжитесь с нами!</h2>
                <hr className="primary" />
                <p><b>Адрес:</b> Астана ул. Бейбитшилик 14, ВП-2</p>
              </div>
              <div className="col-lg-4 col-lg-offset-2 text-center">
                <i className="fa fa-phone fa-3x sr-contact" />
                <p>+7 707 960 65 65</p>
              </div>
              <div className="col-lg-4 text-center">
                <i className="fa fa-envelope-o fa-3x sr-contact" />
                <p><a href="mailto:offkz@yandex.ru">offkz@yandex.ru</a></p>
              </div>
            </div>
          </div>
        </section>
        {/* jQuery */}
        {/* Bootstrap Core JavaScript */}
        {/* Plugin JavaScript */}
        {/* Theme JavaScript */}
      </div>
    );
  }
}
