import React, {Component} from 'react';
import {addScript, qs, qsa} from '../lib/coreLib.js';

export default class Success extends Component {
  componentDidMount() {
    /*
      <script src="vendor/jquery/jquery.min.js"></script>

      <!-- Bootstrap Core JavaScript -->
      <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

      <!-- Plugin JavaScript -->
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
      <script src="vendor/scrollreveal/scrollreveal.min.js"></script>
      <script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

      <!-- Theme JavaScript -->
      <script src="js/creative.min.js"></script>
    */
    const scripts = [
      { src: 'assets/vendor/jquery/jquery.min.js'},
      { src: 'assets/vendor/bootstrap/js/bootstrap.min.js'},
      { src: 'https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'},
      { src: 'assets/vendor/scrollreveal/scrollreveal.min.js'},
      { src: 'assets/vendor/magnific-popup/jquery.magnific-popup.min.js'},
      { src: 'assets/js/creative.min.js'},
    ]

    scripts.forEach((script) => {
      addScript(script);
    });
  }

  render() {
    return (
        <div>
          <nav id="mainNav" className="navbar navbar-default navbar-fixed-top">
            <div className="container-fluid">
              {/* Brand and toggle get grouped for better mobile display */}
              <div className="navbar-header">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span className="sr-only">Toggle navigation</span> Menu <i className="fa fa-bars" />
                </button>
                <a className="navbar-brand page-scroll" href="#page-top">ОФ ФЗ</a>
              </div>
              {/* Collect the nav links, forms, and other content for toggling */}
              <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul className="nav navbar-nav navbar-right">
                  <li>
                    <a className="page-scroll" href="#about">Выберите способ оплаты</a>
                  </li>
                  <li>
                    <a className="page-scroll" href="#contact">Контакты</a>
                  </li>
                </ul>
              </div>
              {/* /.navbar-collapse */}
            </div>
            {/* /.container-fluid */}
          </nav>
          <header style={{height: '450px'}}>
            <div className="header-content">
              <div style={{background: 'red'}} className="header-content-inner">
                <p>
                  <hr/>
                  Извините, возникла ошибка!
                  <hr/>
                  Sorry, an error occurred!
                  <hr/>
                </p>
              </div>
            </div>
          </header>
          <section id="contact">
            <div className="container">
              <div className="row">
                <div className="col-lg-8 col-lg-offset-2 text-center">
                  <h2 className="section-heading">Свяжитесь с нами!</h2>
                  <hr className="primary" />
                  <p><b>Адрес:</b> Астана ул. Бейбитшилик 14, ВП-2</p>
                </div>
                <div className="col-lg-4 col-lg-offset-2 text-center">
                  <i className="fa fa-phone fa-3x sr-contact" />
                  <p>+7 707 960 65 65</p>
                </div>
                <div className="col-lg-4 text-center">
                  <i className="fa fa-envelope-o fa-3x sr-contact" />
                  <p><a href="mailto:offkz@yandex.ru">offkz@yandex.ru</a></p>
                </div>
              </div>
            </div>
          </section>
        </div>
    );
  }
}
